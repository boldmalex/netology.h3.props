import react from "react";
import propTypes from "prop-types";
import "./list-item.css";



const ListItem = ({url, MainImage, title, currency_code, price, quantity}) =>{
    
    const getQuantityClassName = (quantity) =>{
        if (quantity < 10)
            return "item-quantity level-low";
        else if (quantity >= 10 && quantity <= 20)
            return "item-quantity level-medium";
        else
            return "item-quantity level-high";
    };


    const getPriceFormat = (currency_code, price) =>{
        if (currency_code ==="GBP")
            return `${price} ${currency_code}`;
        else
            return `${currency_code} ${price}`;
    }


    return (
            <div className="item">
                <div className="item-image">
                    <a href={url}>
                    <img src={MainImage.url_570xN} alt={MainImage.url_570xN}/>
                    </a>
                </div>
                <div className="item-details">
                    <p className="item-title">{title.length > 50 ? title.substring(0, 49) + "..." : title}</p>
                    <p className="item-price">{getPriceFormat(currency_code, price)}</p>
                    <p className={getQuantityClassName(quantity)}>{quantity} left</p>
                </div>
            </div>
    );
};


ListItem.defaultProps = {
    MainImage : {url_570xN: "no pic"},
    title:"not found",
    quantity: 0,
    currency_code: "",
    price: 0
};


export default ListItem;