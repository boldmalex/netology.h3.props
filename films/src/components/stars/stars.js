import react from "react";
import propTypes from "prop-types";
import Star from "../star/star.js";
import "./stars.css";


const Stars = ({count}) =>{


    return(
            <>
                {count > 1 && count <= 5 && <ul> {Array.from({length:count}, (v,k) => <Star key={k}/>)} </ul>}
            </>
    );
};

Stars.defaultProps = {
    count: 0
};


export default Stars;