import react from "react";
import Listing from "./components/listing/listing.js";
import items from "./Data.js";



function App() {

  return (
      <>
        <Listing items = {items}/>
      </>
  );
}

export default App;
