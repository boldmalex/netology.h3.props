import react from "react";
import PropTypes from "prop-types";
import { object } from "prop-types";
import ListItem from "../list-item/list-item";
import "./listing.css";

const Listing = ({items}) =>{

    if (items.length > 0)
        return (
            <div className="item-list">
                {items.map(item => <ListItem key={item.listing_id} 
                                            url = {item.url} 
                                            MainImage = {item.MainImage} 
                                            title = {item.title} 
                                            currency_code = {item.currency_code} 
                                            price = {item.price} 
                                            quantity ={item.quantity}/>)}
            </div>
        );
    else
        return null;
};

Listing.defaultProps = {
    items: []
};

Listing.propTypes = {
    items: PropTypes.arrayOf(object).isRequired
}

export default Listing;